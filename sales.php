<?php
/*
Plugin Name: sales
Plugin URI: http://www.kurisaka.com
Description: sales
Author: R.K
Version: 1.1
Author URI: http://www.kurisaka.com
*/
class Sales {
	/*
	 * コンストラクタ
	 */
	function __construct() {
		// 管理メニューを追加
		add_action('admin_menu', array($this, 'add_init'));

		// CSS・JSを追加
		//管理画面以外であればjQueryを登録しなおす
		if(!is_admin()){
			wp_enqueue_script('jquery.min.js', 'http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js', array('jquery'), '1.0');
		}
		// PHPファイルを追加読み込み
		require_once 'sales-dao.php';
		add_action('init', array($this, "sales_activate"));
		add_shortcode('sales', array($this, "sales_shortcode"));
	}

	/**
	 * 初期呼び出し
	 */
	function add_init(){
		// 管理メニューを追加
		add_menu_page('営業','営業', 'level_8', __FILE__, array($this,'mst_agt'), '', 26);
		add_submenu_page(__FILE__, 'じゃらんエリア設定', 'じゃらんエリア設定', 8, 'area_entry', array($this,'area_entry'));
		add_submenu_page(__FILE__, '大エリア設定', '大エリア設定', 8, 'large_area_entry', array($this,'large_area_entry'));
		add_submenu_page(__FILE__, 'じゃらんホテル登録', 'じゃらんホテル登録', 8, 'hotel_entry', array($this,'hotel_entry'));
		add_submenu_page(__FILE__, '楽天エリア設定', '楽天エリア設定', 8, 'rakuten_area_entry', array($this,'rakuten_area_entry'));
		add_submenu_page(__FILE__, '楽天ホテル登録', '楽天ホテル登録', 8, 'rakuten_entry_json', array($this,'rakuten_entry_json'));
		add_submenu_page(__FILE__, '出力', '出力', 8, 'output', array($this,'output'));
		add_submenu_page(__FILE__, '設定', '設定', 8, 'agt_settings', array($this,'agt_settings'));
		// エラー出力する場合
		ini_set('display_errors', 1);
	}

	function sales_activate() {
		$installed_ver = get_option('sales_version');
		$db_version = "1.1";

		if($installed_ver != $db_version) {
			global $wpdb;

			$sql1 = "CREATE TABLE {$wpdb->prefix}sales_jalan_area (
						prefecture_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
						prefecture_name NVARCHAR(250) NOT NULL,
						prefecture_code NVARCHAR(10) NOT NULL,
						prefecture_order INT NULL,
						UNIQUE UNIQUE_INDEX_1(prefecture_code))
						DEFAULT CHARACTER SET utf8 collate utf8_unicode_ci,
						ENGINE=InnoDB;";

			$sql2 = "CREATE TABLE {$wpdb->prefix}sales_jalan_large_area (
						large_area_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
						large_area_name NVARCHAR(250) NOT NULL,
						large_area_code NVARCHAR(10) NOT NULL,
						prefecture_code NVARCHAR(10) NOT NULL,
						large_area_order INT NULL,
						UNIQUE UNIQUE_INDEX_2(large_area_code),
						FOREIGN KEY (prefecture_code) 
						REFERENCES {$wpdb->prefix}sales_jalan_area(prefecture_code) 
						ON UPDATE CASCADE 
						ON DELETE CASCADE)
						DEFAULT CHARACTER SET utf8 collate utf8_unicode_ci,
						ENGINE=InnoDB;";

			$sql3 = "CREATE TABLE {$wpdb->prefix}sales_jalan_hotel (
						hotel_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
						hotel_name NVARCHAR(250) NOT NULL,
						hotel_code INT NULL,
						post_code NVARCHAR(50) NOT NULL,
						hotel_address NVARCHAR(250) NOT NULL,
						hotel_type NVARCHAR(250) NOT NULL,
						region NVARCHAR(250) NOT NULL,
						prefecture NVARCHAR(250) NOT NULL,
						large_area NVARCHAR(250) NOT NULL,
						small_area NVARCHAR(250) NOT NULL,
						prefecture_code NVARCHAR(10) NULL,
						large_area_code NVARCHAR(10) NULL,
						number_of_ratings INT NOT NULL,
						rating DOUBLE NOT NULL,
						hotel_order INT NULL,
						entry_date TIMESTAMP NOT NULL,
						UNIQUE UNIQUE_INDEX_4(hotel_code))
						DEFAULT CHARACTER SET utf8 collate utf8_unicode_ci,
						ENGINE=InnoDB;";

			$sql4 = "CREATE TABLE {$wpdb->prefix}sales_rakuten_area (
						area_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
						large_name NVARCHAR(250) NOT NULL,
						large_code NVARCHAR(50) NOT NULL,
						middle_name NVARCHAR(250) NULL,
						middle_code NVARCHAR(50) NULL,
						small_name NVARCHAR(250) NULL,
						small_code NVARCHAR(50) NULL,
						detail_name NVARCHAR(250) NULL,
						detail_code NVARCHAR(50) NULL,
						status ENUM('0','1') NULL DEFAULT '0',
						area_order INT NULL)
						DEFAULT CHARACTER SET utf8 collate utf8_unicode_ci,
						ENGINE=InnoDB;";

			$sql5 = "CREATE TABLE {$wpdb->prefix}sales_rakuten_hotel (
						hotel_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
						hotel_name NVARCHAR(250) NOT NULL,
						hotel_code INT NOT NULL,
						post_code NVARCHAR(50) NULL,
						hotel_address NVARCHAR(250) NULL,
						hotel_type NVARCHAR(250) NULL,
						room_num NVARCHAR(10) NULL,
						prefecture NVARCHAR(250) NULL,
						large_area NVARCHAR(250) NULL,
						small_area NVARCHAR(250) NULL,
						tel NVARCHAR(20) NULL,
						fax NVARCHAR(20) NULL,
						number_of_ratings INT NULL,
						rating DOUBLE NULL,
						service_average DOUBLE NULL,
						location_average DOUBLE NULL,
						room_average DOUBLE NULL,
						equipment_average DOUBLE NULL,
						bath_average DOUBLE NULL,
						meal_average DOUBLE NULL,
						hotel_order INT NULL,
						entry_date TIMESTAMP NOT NULL,
						UNIQUE UNIQUE_INDEX_3(hotel_code))
						DEFAULT CHARACTER SET utf8 collate utf8_unicode_ci,
						ENGINE=InnoDB;";

			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

			dbDelta($sql1." ".$sql2." ".$sql3." ".$sql4." ".$sql5);
			update_option('sales_version', $db_version);
		}
	}

	function sales_shortcode($atts) {
		extract(shortcode_atts(array(
			'page' => 0,
			'month' => 0,
			'random' => 0,
		), $atts));

		if ($page == "jalan") {
			output_jalan();
		} else {
			output_rakuten();
		}
	}

	function agt_settings() {
		?>
			<div class="wrap">
				<h2>設定
					<a href="javascript:add_area();" class="add-new-h2">新規追加</a>
				</h2>
				<form method="post" action="" name="form1">
					<table class="form-table">
						<tbody>
							<tr>
								<th scope="row">じゃらんnetエリア登録</th>
								<td>

								</td>
							</tr>
							<tr>
								<th scope="row">じゃらんnetホテル登録</th>
								<td>

								</td>
							</tr>
							<tr>
								<th scope="row">楽天トラベルエリア登録</th>
								<td>
	
								</td>
							</tr>
							<tr>
								<th scope="row">楽天トラベルホテル登録</th>
								<td>
								</td>
							</tr>

						</tbody>
					</table>
					<input type="submit" name="new" class="button button-primary" value="登録">
				</form>
			</div>
		<?php
	}

	function rakuten_area_entry() {
		$dao = new SalesDao();
		$xml = get_rakuten_area();
		//var_dump($xml);

		for ($i = 0; $i < count($xml->areaClasses->largeClasses->largeClass
			->middleClasses->middleClass); $i++) {

			// smallClassでループ
			for ($j = 0; $j < count($xml->areaClasses->largeClasses->largeClass
				->middleClasses->middleClass[$i]
				->smallClasses->smallClass); $j++) {

				// detailClassでループ
				if (count($xml->areaClasses->largeClasses->largeClass
											->middleClasses->middleClass[$i]
											->smallClasses->smallClass[$j]
											->detailClasses->detailClass) > 0) {

					for ($k = 0; $k < count($xml->areaClasses->largeClasses->largeClass
											->middleClasses->middleClass[$i]
											->smallClasses->smallClass[$j]
											->detailClasses->detailClass); $k++) {

						$dao->insert_rakuten_area($xml->areaClasses
													->largeClasses->largeClass
													->largeClassName,
													$xml->areaClasses
													->largeClasses->largeClass
													->largeClassCode,
													$xml->areaClasses->largeClasses->largeClass
													->middleClasses->middleClass[$i]
													->middleClassName,
													$xml->areaClasses->largeClasses->largeClass
													->middleClasses->middleClass[$i]
													->middleClassCode,
													$xml->areaClasses->largeClasses->largeClass
													->middleClasses->middleClass[$i]
													->smallClasses->smallClass[$j]
													->smallClassName,
													$xml->areaClasses->largeClasses->largeClass
													->middleClasses->middleClass[$i]
													->smallClasses->smallClass[$j]
													->smallClassCode,
													$xml->areaClasses->largeClasses->largeClass
													->middleClasses->middleClass[$i]
													->smallClasses->smallClass[$j]
													->detailClasses->detailClass[$k]
													->detailClassName,
													$xml->areaClasses->largeClasses->largeClass
													->middleClasses->middleClass[$i]
													->smallClasses->smallClass[$j]
													->detailClasses->detailClass[$k]
													->detailClassCode,
													0,
													$k+1);
					}
					
				} else {
					//smallClassのインサートを記入
					$dao->insert_rakuten_area($xml->areaClasses
												->largeClasses->largeClass
												->largeClassName,
												$xml->areaClasses
												->largeClasses->largeClass
												->largeClassCode,
												$xml->areaClasses->largeClasses->largeClass
												->middleClasses->middleClass[$i]
												->middleClassName,
												$xml->areaClasses->largeClasses->largeClass
												->middleClasses->middleClass[$i]
												->middleClassCode,
												$xml->areaClasses->largeClasses->largeClass
												->middleClasses->middleClass[$i]
												->smallClasses->smallClass[$j]
												->smallClassName,
												$xml->areaClasses->largeClasses->largeClass
												->middleClasses->middleClass[$i]
												->smallClasses->smallClass[$j]
												->smallClassCode,
												"",
												"",
												0,
												$j+1);
				}
			}
		}
	}

	function area_entry() {
		$dao = new SalesDao();
		$xml = get_jalan_area();
		for ($i = 0; $i < count($xml->Prefecture); $i++) {
			$dao->insert_jalan_area($xml->Prefecture[$i]['name'],
									$xml->Prefecture[$i]['cd'],
									$i);
		}
	}

	function large_area_entry() {
		$dao = new SalesDao();
		$xml = get_jalan_area();
		for ($i = 0; $i < count($xml->Prefecture); $i++) {
			for ($j = 0; $j < count($xml->Prefecture[$i]->LargeArea); $j++) {
				$dao->insert_jalan_large_area($xml->Prefecture[$i]->LargeArea[$j]['name'],
												$xml->Prefecture[$i]->LargeArea[$j]['cd'],
												$xml->Prefecture[$i]['cd'],
												$j+1);
			}
		}
		echo "end";
	}

	function hotel_entry() {
		$dao = new SalesDao();
		//$area = $dao->all_jalan_area();
		$area = $dao->all_jalan_large_area();
		//$area_count = 5;
		$area_count = count($area);
		// timestampを作成
		$time = time() + 9*3600;
		$entry_date =date("Y-m-d H:i:s", $time);

		for ($i = 0; $i < $area_count; $i++) {
			// エリア件数を取得（エリア、人気順、件数、スタート位置）
			//$result = get_jalan_prefecture_hotel($area[$i]->prefecture_code, 4, 1, 1);
			$result = get_jalan_large_area_hotel($area[$i]->large_area_code, 4, 1, 1);

			// エリアの合計数を取得
			$hotel_count = $result->NumberOfResults;

			// 100で割って切り上げしてループ数を取得
			$loop_count = ceil($hotel_count / 100);

			//$loop_count = 1;
			for ($j = 0; $j < $loop_count; $j++) {
				$start = ($j * 100) + 1;
				//$hotel = get_jalan_prefecture_hotel($area[$i]->prefecture_code, 4, 100, $start);
				$hotel = get_jalan_large_area_hotel($area[$i]->large_area_code, 4, 100, $start);
				
				for ($k = 0; $k < 100; $k++) {
					// Insert
					$order = $start + $k;
					$dao->insert_jalan_hotel($hotel->Hotel[$k]->HotelName,
												$hotel->Hotel[$k]->HotelID,
												$hotel->Hotel[$k]->PostCode,
												$hotel->Hotel[$k]->HotelAddress,
												$hotel->Hotel[$k]->HotelType,
												$hotel->Hotel[$k]->Area->Region,
												$hotel->Hotel[$k]->Area->Prefecture,
												$hotel->Hotel[$k]->Area->LargeArea,
												$hotel->Hotel[$k]->Area->SmallArea,
												$area[$i]->prefecture_code,
												$area[$i]->large_area_code,
												$hotel->Hotel[$k]->NumberOfRatings,
												$hotel->Hotel[$k]->Rating,
												$order,
												$entry_date);
				}
				
			}
		}
		echo "終了". date('Y-m-d H:i:s');
	}

	function rakuten_entry_xml() {
		$dao = new SalesDao();
		//$area = $dao->all_jalan_area();
		$area = $dao->all_rakuten_area();
		//$area_count = 5;
		$area_count = count($area);
		// timestampを作成
		$time = time() + 9*3600;
		$entry_date =date("Y-m-d H:i:s", $time);

		for ($i = 0; $i < $area_count; $i++) {
			if ($area[$i]->status == 1) { continue; }
			// 詳細エリアがあるかどうか
			if ($area[$i]->detail_code == "") {
				// 詳細エリアなし
				$result = get_rakuten_small_area_hotel($area[$i]->middle_code, $area[$i]->small_code, 1);
				// エリアの合計数を取得
				$hotel_count = $result->pagingInfo->last - $result->pagingInfo->first;
				$loop_count = $result->pagingInfo->pageCount;
				$start = $result->pagingInfo->first;

				for ($j = 0; $j < $loop_count; $j++) {
					// 楽天よりデータ取得
					$hotel = get_rakuten_small_area_hotel($area[$i]->middle_code, $area[$i]->small_code, $j+1);

					for ($k = 0; $k < $hotel_count; $k++) {
						// Insert
						$start = $start + $k;
						$dao->insert_rakuten_hotel($hotel->hotels->hotel[$k]->hotelBasicInfo->hotelName,
													$hotel->hotels->hotel[$k]->hotelBasicInfo->hotelNo,
													$hotel->hotels->hotel[$k]->hotelBasicInfo->postalCode,
													$hotel->hotels->hotel[$k]->hotelBasicInfo->address1.$hotel->hotels->hotel[$k]->hotelBasicInfo->address2,
													$hotel->hotels->hotel[$k]->hotelDetailInfo->hotelClassCode,
													$hotel->hotels->hotel[$k]->hotelDetailInfo->middleClassCode,
													$hotel->hotels->hotel[$k]->hotelDetailInfo->smallClassCode,
													$hotel->hotels->hotel[$k]->hotelDetailInfo->areaName,
													$hotel->hotels->hotel[$k]->hotelBasicInfo->telephoneNo,
													$hotel->hotels->hotel[$k]->hotelBasicInfo->faxNo,
													$hotel->hotels->hotel[$k]->hotelBasicInfo->reviewCount,
													$hotel->hotels->hotel[$k]->hotelBasicInfo->reviewAverage,
													$hotel->hotels->hotel[$k]->hotelRatingInfo->serviceAverage,
													$hotel->hotels->hotel[$k]->hotelRatingInfo->locationAverage,
													$hotel->hotels->hotel[$k]->hotelRatingInfo->roomAverage,
													$hotel->hotels->hotel[$k]->hotelRatingInfo->equipmentAverage,
													$hotel->hotels->hotel[$k]->hotelRatingInfo->bathAverage,
													$hotel->hotels->hotel[$k]->hotelRatingInfo->mealAverage,
													$start,
													$entry_date);
						$start++;
						//sleep(1);
						echo $hotel->hotels->hotel[$k]->hotelBasicInfo->hotelNo."<br>";
					}
					
				}
				// 取得済エリアとする
				$dao->update_rakuten_area($area[$i]->area_id,
												$area[$i]->large_name,
												$area[$i]->large_code,
												$area[$i]->middle_name,
												$area[$i]->middle_code,
												$area[$i]->small_name,
												$area[$i]->small_code,
												$area[$i]->detail_name,
												$area[$i]->detail_code,
												"1",
												$area[$i]->area_order);
			} else {
				// 詳細エリアあり
				$result = get_rakuten_detail_area_hotel($area[$i]->middle_code, $area[$i]->small_code, $area[$i]->detail_code, 1);
				// エリアの合計数を取得
				$hotel_count = $result->pagingInfo->last - $result->pagingInfo->first;
				$loop_count = $result->pagingInfo->pageCount;
				$start = $result->pagingInfo->first;

				for ($j = 0; $j < $loop_count; $j++) {
					// 楽天よりデータ取得
					$hotel = get_rakuten_detail_area_hotel($area[$i]->middle_code, $area[$i]->small_code, $area[$i]->detail_code, $j+1);

					for ($k = 0; $k < $hotel_count; $k++) {
						// Insert
						$start = $start + $k;
						$dao->insert_rakuten_hotel($hotel->hotels->hotel[$k]->hotelBasicInfo->hotelName,
													$hotel->hotels->hotel[$k]->hotelBasicInfo->hotelNo,
													$hotel->hotels->hotel[$k]->hotelBasicInfo->postalCode,
													$hotel->hotels->hotel[$k]->hotelBasicInfo->address1.$hotel->hotels->hotel[$k]->hotelBasicInfo->address2,
													$hotel->hotels->hotel[$k]->hotelDetailInfo->hotelClassCode,
													$hotel->hotels->hotel[$k]->hotelDetailInfo->middleClassCode,
													$hotel->hotels->hotel[$k]->hotelDetailInfo->smallClassCode,
													$hotel->hotels->hotel[$k]->hotelDetailInfo->areaName,
													$hotel->hotels->hotel[$k]->hotelBasicInfo->telephoneNo,
													$hotel->hotels->hotel[$k]->hotelBasicInfo->faxNo,
													$hotel->hotels->hotel[$k]->hotelBasicInfo->reviewCount,
													$hotel->hotels->hotel[$k]->hotelBasicInfo->reviewAverage,
													$hotel->hotels->hotel[$k]->hotelRatingInfo->serviceAverage,
													$hotel->hotels->hotel[$k]->hotelRatingInfo->locationAverage,
													$hotel->hotels->hotel[$k]->hotelRatingInfo->roomAverage,
													$hotel->hotels->hotel[$k]->hotelRatingInfo->equipmentAverage,
													$hotel->hotels->hotel[$k]->hotelRatingInfo->bathAverage,
													$hotel->hotels->hotel[$k]->hotelRatingInfo->mealAverage,
													$start,
													$entry_date);
						$start++;
						//sleep(1);
						echo $hotel->hotels->hotel[$k]->hotelBasicInfo->hotelNo."<br>";
					}
					
				}
				// 取得済エリアとする
				$dao->update_rakuten_area($area[$i]->area_id,
												$area[$i]->large_name,
												$area[$i]->large_code,
												$area[$i]->middle_name,
												$area[$i]->middle_code,
												$area[$i]->small_name,
												$area[$i]->small_code,
												$area[$i]->detail_name,
												$area[$i]->detail_code,
												"1",
												$area[$i]->area_order);
			}


		}
		echo "終了". date('Y-m-d H:i:s');
	}

	function rakuten_entry_json() {
		$dao = new SalesDao();
		//$area = $dao->all_jalan_area();
		$area = $dao->all_rakuten_area();
		//$area_count = 5;
		$area_count = count($area);
		// timestampを作成
		$time = time() + 9*3600;
		$entry_date =date("Y-m-d H:i:s", $time);

		for ($i = 0; $i < $area_count; $i++) {
			if ($area[$i]->status == 1) { continue; }
			// 詳細エリアがあるかどうか
			if ($area[$i]->detail_code == "") {
				// 詳細エリアなし
				$result = get_rakuten_small_area_hotel_json($area[$i]->middle_code, $area[$i]->small_code, 1);
				// エリアの合計数を取得
				$hotel_count = $result["pagingInfo"]["last"];
				$loop_count = $result["pagingInfo"]["pageCount"];
				$start = $result["pagingInfo"]["first"];

				for ($j = 0; $j < $loop_count; $j++) {
					// 楽天よりデータ取得
					$hotel = get_rakuten_small_area_hotel_json($area[$i]->middle_code, $area[$i]->small_code, $j+1);
					//var_dump($hotel);

					for ($k = 0; $k < $hotel_count; $k++) {
						// Insert
						$start = $start + $k;

						$dao->insert_rakuten_hotel($hotel["hotels"][$k]["hotel"][0]["hotelBasicInfo"]["hotelName"],
													$hotel["hotels"][$k]["hotel"][0]["hotelBasicInfo"]["hotelNo"],
													$hotel["hotels"][$k]["hotel"][0]["hotelBasicInfo"]["postalCode"],
													$hotel["hotels"][$k]["hotel"][0]["hotelBasicInfo"]["address1"].$hotel["hotels"][$k]["hotel"][0]["hotelBasicInfo"]["address2"],
													$hotel["hotels"][$k]["hotel"][2]["hotelDetailInfo"]["hotelClassCode"],
													$hotel["hotels"][$k]["hotel"][3]["hotelFacilitiesInfo"]["hotelRoomNum"],
													$hotel["hotels"][$k]["hotel"][2]["hotelDetailInfo"]["middleClassCode"],
													$hotel["hotels"][$k]["hotel"][2]["hotelDetailInfo"]["smallClassCode"],
													$hotel["hotels"][$k]["hotel"][2]["hotelDetailInfo"]["areaName"],
													$hotel["hotels"][$k]["hotel"][0]["hotelBasicInfo"]["telephoneNo"],
													$hotel["hotels"][$k]["hotel"][0]["hotelBasicInfo"]["faxNo"],
													$hotel["hotels"][$k]["hotel"][0]["hotelBasicInfo"]["reviewCount"],
													$hotel["hotels"][$k]["hotel"][0]["hotelBasicInfo"]["reviewAverage"],
													$hotel["hotels"][$k]["hotel"][1]["hotelRatingInfo"]["serviceAverage"],
													$hotel["hotels"][$k]["hotel"][1]["hotelRatingInfo"]["locationAverage"],
													$hotel["hotels"][$k]["hotel"][1]["hotelRatingInfo"]["roomAverage"],
													$hotel["hotels"][$k]["hotel"][1]["hotelRatingInfo"]["equipmentAverage"],
													$hotel["hotels"][$k]["hotel"][1]["hotelRatingInfo"]["bathAverage"],
													$hotel["hotels"][$k]["hotel"][1]["hotelRatingInfo"]["mealAverage"],
													$start,
													$entry_date);

						$start++;
						//sleep(1);
						echo $hotel["hotels"][$k]["hotel"][0]["hotelBasicInfo"]["hotelNo"]."<br>";
					}
					
				}
				// 取得済エリアとする
				$dao->update_rakuten_area($area[$i]->area_id,
												$area[$i]->large_name,
												$area[$i]->large_code,
												$area[$i]->middle_name,
												$area[$i]->middle_code,
												$area[$i]->small_name,
												$area[$i]->small_code,
												$area[$i]->detail_name,
												$area[$i]->detail_code,
												"1",
												$area[$i]->area_order);
			} else {
				// 詳細エリアあり
				$result = get_rakuten_detail_area_hotel_json($area[$i]->middle_code, $area[$i]->small_code, $area[$i]->detail_code, 1);
				// エリアの合計数を取得
				$hotel_count = $result["pagingInfo"]["last"];
				$loop_count = $result["pagingInfo"]["pageCount"];
				$start = $result["pagingInfo"]["first"];

				for ($j = 0; $j < $loop_count; $j++) {
					// 楽天よりデータ取得
					$hotel = get_rakuten_detail_area_hotel_json($area[$i]->middle_code, $area[$i]->small_code, $area[$i]->detail_code, $j+1);

					for ($k = 0; $k < $hotel_count; $k++) {
						// Insert
						$start = $start + $k;

						$dao->insert_rakuten_hotel($hotel["hotels"][$k]["hotel"][0]["hotelBasicInfo"]["hotelName"],
													$hotel["hotels"][$k]["hotel"][0]["hotelBasicInfo"]["hotelNo"],
													$hotel["hotels"][$k]["hotel"][0]["hotelBasicInfo"]["postalCode"],
													$hotel["hotels"][$k]["hotel"][0]["hotelBasicInfo"]["address1"].$hotel["hotels"][$k]["hotel"][0]["hotelBasicInfo"]["address2"],
													$hotel["hotels"][$k]["hotel"][2]["hotelDetailInfo"]["hotelClassCode"],
													$hotel["hotels"][$k]["hotel"][3]["hotelFacilitiesInfo"]["hotelRoomNum"],
													$hotel["hotels"][$k]["hotel"][2]["hotelDetailInfo"]["middleClassCode"],
													$hotel["hotels"][$k]["hotel"][2]["hotelDetailInfo"]["smallClassCode"],
													$hotel["hotels"][$k]["hotel"][2]["hotelDetailInfo"]["areaName"],
													$hotel["hotels"][$k]["hotel"][0]["hotelBasicInfo"]["telephoneNo"],
													$hotel["hotels"][$k]["hotel"][0]["hotelBasicInfo"]["faxNo"],
													$hotel["hotels"][$k]["hotel"][0]["hotelBasicInfo"]["reviewCount"],
													$hotel["hotels"][$k]["hotel"][0]["hotelBasicInfo"]["reviewAverage"],
													$hotel["hotels"][$k]["hotel"][1]["hotelRatingInfo"]["serviceAverage"],
													$hotel["hotels"][$k]["hotel"][1]["hotelRatingInfo"]["locationAverage"],
													$hotel["hotels"][$k]["hotel"][1]["hotelRatingInfo"]["roomAverage"],
													$hotel["hotels"][$k]["hotel"][1]["hotelRatingInfo"]["equipmentAverage"],
													$hotel["hotels"][$k]["hotel"][1]["hotelRatingInfo"]["bathAverage"],
													$hotel["hotels"][$k]["hotel"][1]["hotelRatingInfo"]["mealAverage"],
													$start,
													$entry_date);
						$start++;
						//sleep(1);
						echo $hotel["hotels"][$k]["hotel"][0]["hotelBasicInfo"]["hotelNo"]."<br>";
					}
					
				}
				// 取得済エリアとする
				$dao->update_rakuten_area($area[$i]->area_id,
												$area[$i]->large_name,
												$area[$i]->large_code,
												$area[$i]->middle_name,
												$area[$i]->middle_code,
												$area[$i]->small_name,
												$area[$i]->small_code,
												$area[$i]->detail_name,
												$area[$i]->detail_code,
												"1",
												$area[$i]->area_order);
			}


		}
		echo "終了". date('Y-m-d H:i:s');
	}

	public function mst_agt() {
		echo "eigyou";
		return "eigyou";
	}

	function mst_category() {
		$dao = new SalesDao();
		$category = $dao->all_agt_category();

		// 登録処理
		if (isset($_POST["entry"])) {
			//var_dump($_POST);

			// 空の配列
			$id_array = array();
			for ($i = 0; $i < count($category); $i++) {
				// category_idの配列を作成
				array_push($id_array, $category[$i]->category_id);
			}

			// 差分の配列を取得
			$diff_id = array_diff($_POST["category_id"], $id_array);
			// keyの配列を取得
			$key = array_keys($diff_id);

			// 新規登録処理
			for ($j = 0; $j < count($diff_id); $j++) {
				$dao->insert_category($_POST["category_name"][$key[$j]], $_POST["category_order"][$key[$j]]);
			}
			// 変更処理
			for ($k = 0; $k < count($_POST["category_id"]); $k++) {
				if (! empty($_POST["category_id"][$k])) {
					$dao->update_category($_POST["category_id"][$k], $_POST["category_name"][$k], $_POST["category_order"][$k]);
				}
			}
		} else {
			// 削除処理
			foreach (array_keys($_POST) as $key => $value) {
				if(strstr($value, 'delete')) {
					// nameからidを抜き出し
					$delete = ltrim($value, "delete");
					$id = "delete" . $delete;
					if (isset($_POST[$id])) {
						$dao->delete_category($delete);
					}
				}
			}
		}

		// 最新情報を取得
		$category = $dao->all_agt_category();
		?>
			<script type="text/javascript">
				function chkForm(oj){
					// 確認ダイアログを表示
					if (window.confirm('登録してよろしいですか？')) {
						return true;
					}
					else {
						return false;
					}
				}
				function add_category(){
					var input = $('<tr>\
									<td>\
										<input name="category_id[]" type="hidden" value="">\
									</td>\
									<td>\
										<input name="category_name[]" type="text" size="20" value="">\
									</td>\
									<td>\
										<input name="category_order[]" type="text" size="5" value="">\
									</td>\
									<td>\
									</td></tr>');
					$("#category_body").append(input);
				}
			</script>
			<div class="wrap">
				<h2>カテゴリ設定
					<a href="javascript:add_category();" class="add-new-h2">新規追加</a>
				</h2>
				<form action="admin.php?page=mst_category" method="post" onsubmit="return chkForm(this)">
					<table class="wp-list-table widefat fixed striped posts">
						<thead>
							<tr>
								<th>category_id</th>
								<th>category_name</th>
								<th>category_order</th>
								<th></th>
							</tr>
						</thead>
						<tbody id="category_body">
							<?php
								for ($i = 0; $i < count($category); $i++) {
									print('<tr>');
									print('<td>'. $category[$i]->category_id. '</td>');
									print('<td>');
									print('<input name="category_id[]" type="hidden" value="'. $category[$i]->category_id. '">');
									print('<input name="category_name[]" type="text" size="20" value="'. $category[$i]->category_name. '">');
									print('</td>');
									print('<td>');
									print('<input name="category_order[]" type="text" size="5" value="'. $category[$i]->category_order. '">');
									print('</td>');
									print('<td>');
									print('<input name="delete'. $category[$i]->category_id. '[]" type="submit" class="button button-primary" value="削除">');
									print('</td>');
									print('</tr>');
								}
							?>
						</tbody>
					</table>
					<p>※category_orderを99に設定すると非表示になります。</p>
					<input type="submit" name= "entry" class="button button-primary" value="登録">
				</form>
			</div>
		<?php
	}
}

// グローバル変数定義
$sales = new Sales();

function output_jalan() {
	$dao = new SalesDao();

	if (isset($_GET["hotel"])) {
		$hotels = $dao->get_jalan_search($_GET["hotel"]);

	} else {
		$hotels = $dao->all_jalan_hotel();
	}
	// Pagerの読み込み
	require_once("Pager/Pager.php");
	$perPage = 50;
	$params = array(
		"perPage"=>$perPage,
		"itemData" => $hotels,
		"prevImg" => "< Previous",
		"nextImg'"  => "Next >",
	);
	$pager = Pager::factory($params);

	?>
		<div class="wrap">
			<h2>じゃらんnetホテル一覧</h2>
			<form action="" name="search_form" method="get" class="form-inline">
				<div id="search-box" >
					<input name="page_id" type="hidden" value="<?php echo $_GET["page_id"]; ?>">
					<input id="hotel" name="hotel" type="text" size="50" value="<?php echo $_GET["hotel"]; ?>" class="form-control search-box" placeholder="ホテル名" style="float:left; margin-right:20px;">

					<!-- クチコミ検索
					<label style="float:left; margin-right:5px;">クチコミ評点</label><input type="number" name="rating" size="5" step="0.1" min="1.0" max="5.0" value="" class="form-control search-box" style="float:left; margin-right:20px;">
					 -->

					<input type="submit" name="get" value="検索" class="search-box btn btn-primary" style="float:left;">
				</div>
			</form>
			<?php
				// Pager出力処理
				print('<div class="pager" style="clear:left;text-align: center;"><br>');
				$navi = $pager->getLinks();
				if (count($hotels) > 9999) {
					$count = $dao->count_jalan_hotel();
					print($count[0]->count."件中 ");
				} else {
					print($pager->numItems()."件中 ");
				}
				print($navi['all']);
				$scope = $pager->getOffsetByPageId();
				$values = $pager->getPageData();
				print('</div>');
				
			?>
			<div class="row">
				<a href="wp-content/plugins/sales/jalan_csv.php?hotel=<?php echo $_GET["hotel"]; ?>" target="_blank" class="btn btn-primary pull-right" style="width: 150px;">CSV出力</a>
			</div>
			<form action="" name="form1" method="get">
				<div class="table-responsive">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>宿番号</th>
								<th>ホテル名</th>
								<th>タイプ</th>
								<th>都道府県</th>
								<th>大エリア</th>
								<th>クチコミ評点</th>
								<th>クチコミ件数</th>
								<th>大エリア人気順</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$i = 1;
							foreach ($values as $value) {
								print('<tr>');
								print('<td>'. $value->hotel_code . '</td>');
								print('<td>'. $value->hotel_name . '</td>');
								print('<td>'. $value->hotel_type . '</td>');
								print('<td>'. $value->prefecture . '</td>');
								print('<td>'. $value->largeArea . '</td>');
								print('<td>'. $value->rating . '</td>');
								print('<td>'. $value->number_of_ratings . '</td>');
								print('<td>'. $value->hotel_order . '</td>');
								print('</tr>');
							}
							?>
						</tbody>
					</table>
				</div>
			</form>
			<?php
				// Pager出力処理
				print('<div class="pager" style="clear:left;text-align: center;"><br>');
				$navi = $pager->getLinks();
				if (count($hotels) > 9999) {
					$count = $dao->count_jalan_hotel();
					print($count[0]->count."件中 ");
				} else {
					print($pager->numItems()."件中 ");
				}
				print($navi['all']);
				$scope = $pager->getOffsetByPageId();
				$values = $pager->getPageData();
				print('</div>');
			?>
		</div>
	<?php
}

function output_rakuten() {
	$dao = new SalesDao();

	if (isset($_GET["hotel"])) {
		$hotels = $dao->get_rakuten_search($_GET["hotel"]);
	} else {
		$hotels = $dao->all_rakuten_hotel();
	}
	// Pagerの読み込み
	require_once("Pager/Pager.php");
	$perPage = 50;
	$params = array(
		"perPage"=>$perPage,
		"itemData" => $hotels,
		"prevImg" => "< Previous",
		"nextImg'"  => "Next >",
	);
	$pager = Pager::factory($params);
	?>
		<div class="wrap">
			<h2>楽天トラベルホテル一覧</h2>
			<form action="" name="search_form" method="get" class="form-inline">
				<div id="search-box" >
					<input name="page_id" type="hidden" value="<?php echo $_GET["page_id"]; ?>">
					<input id="hotel" name="hotel" type="text" size="50" value="<?php echo $_GET["hotel"]; ?>" class="form-control search-box" placeholder="ホテル名" style="float:left; margin-right:10px;">
					<input type="submit" name="get" value="検索" class="search-box btn btn-primary" style="float:left;">
				</div>
			</form>
			<?php
				// Pager出力処理
				print('<div class="pager" style="clear:left;text-align: center;"><br>');
				$navi = $pager->getLinks();
				if (count($hotels) > 9999) {
					$count = $dao->count_rakuten_hotel();
					print($count[0]->count."件中 ");
				} else {
					print($pager->numItems()."件中 ");
				}
				print($navi['all']);
				$scope = $pager->getOffsetByPageId();
				$values = $pager->getPageData();
				print('</div>');
			?>
			<div class="row">
				<a href="wp-content/plugins/sales/rakuten_csv.php?hotel=<?php echo $_GET["hotel"]; ?>" target="_blank" class="btn btn-primary pull-right" style="width: 150px;">CSV出力</a>
			</div>
			<form action="" name="form1" method="get">
				<div class="table-responsive">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>宿番号</th>
								<th>ホテル名</th>
								<th>タイプ</th>
								<th>エリア</th>
								<th>クチコミ評点</th>
								<th>クチコミ件数</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$i = 1;
							foreach ($values as $value) {
								print('<tr>');
								print('<td>'. $value->hotel_code . '</td>');
								print('<td>'. $value->hotel_name . '</td>');
								print('<td>'. $value->hotel_type . '</td>');
								print('<td>'. $value->small_area . '</td>');
								print('<td>'. $value->rating . '</td>');
								print('<td>'. $value->number_of_ratings . '</td>');
								print('</tr>');
							}
							?>
						</tbody>
					</table>
				</div>
			</form>
			<?php
				// Pager出力処理
				print('<div class="pager" style="clear:left;text-align: center;"><br>');
				$navi = $pager->getLinks();
				if (count($hotels) > 9999) {
					$count = $dao->count_rakuten_hotel();
					print($count[0]->count."件中 ");
				} else {
					print($pager->numItems()."件中 ");
				}
				print($navi['all']);
				$scope = $pager->getOffsetByPageId();
				$values = $pager->getPageData();
				print('</div>');
			?>
		</div>
	<?php
}

function get_rakuten_area() {
	// XMLデータ取得用ベースURL
	$req = "https://app.rakuten.co.jp/services/api/Travel/GetAreaClass/20131024?applicationId=1010215401048272190&&format=xml";
	// XMLファイルをパースし、オブジェクトを取得(＠でエラーを回避)
	$xml = @simplexml_load_file($req) or die("XMLパースエラー");
	//var_dump ($xml);
	return $xml;
}

function get_rakuten_detail_area_hotel($middle, $small, $detail, $page) {
	// XMLデータ取得用ベースURL
	// middleClassCode：中エリア
	// smallClassCode：小エリア
	// detailClassCode：詳細エリア
	// page：ページ番号
	$req = "https://app.rakuten.co.jp/services/api/Travel/SimpleHotelSearch/20131024?".
			"applicationId=1010215401048272190&format=xml&largeClassCode=japan". 
			"&middleClassCode=" . $middle ."&smallClassCode=". $small .
			"&detailClassCode=". $detail ."&responseType=large&page=".$page;

	// XMLファイルをパースし、オブジェクトを取得(＠でエラーを回避)
	$xml = @simplexml_load_file($req) or die($xml = @simplexml_load_file($req) or die("XMLパースエラーdetail:".$req.$xml));
	//var_dump ($xml);
	return $xml;
}

function get_rakuten_detail_area_hotel_json($middle, $small, $detail, $page) {

	try {
		// XMLデータ取得用ベースURL
		// middleClassCode：中エリア
		// smallClassCode：小エリア
		// detailClassCode：詳細エリア
		// page：ページ番号
		$req = "https://app.rakuten.co.jp/services/api/Travel/SimpleHotelSearch/20131024?".
				"applicationId=1010215401048272190&format=json&largeClassCode=japan". 
				"&middleClassCode=" . $middle ."&smallClassCode=". $small .
				"&detailClassCode=". $detail ."&responseType=large&page=".$page;
		$json = file_get_contents($req);
		// JSONファイルは配列に変換しておく
		$arr = json_decode($json,true);
	} catch(Exception $err) {
		sleep(5);
		$arr = @json_decode($json,true) or die("jsonパースエラーsmall:".$req.$json);
	}
	return $arr;
}

function get_rakuten_small_area_hotel($middle, $small, $page) {
	try {
		// XMLデータ取得用ベースURL
		// middleClassCode：中エリア
		// smallClassCode：小エリア
		// detailClassCode：詳細エリア
		// page：ページ番号
		$req = "https://app.rakuten.co.jp/services/api/Travel/SimpleHotelSearch/20131024?".
				"applicationId=1010215401048272190&format=xml&largeClassCode=japan". 
				"&middleClassCode=" . $middle ."&smallClassCode=". $small .
				"&responseType=large&page=".$page;

		// XMLファイルをパースし、オブジェクトを取得(＠でエラーを回避)
		$xml = @simplexml_load_file($req);
		//var_dump ($xml);
	} catch(Exception $err) {
		sleep(5);
		$xml = @simplexml_load_file($req) or die("XMLパースエラーsmall:".$req.$xml);
	}
	return $xml;
}

function get_rakuten_small_area_hotel_json($middle, $small, $page) {
	try {
		// XMLデータ取得用ベースURL
		// middleClassCode：中エリア
		// smallClassCode：小エリア
		// detailClassCode：詳細エリア
		// page：ページ番号
		$req = "https://app.rakuten.co.jp/services/api/Travel/SimpleHotelSearch/20131024?".
				"applicationId=1010215401048272190&format=json&largeClassCode=japan". 
				"&middleClassCode=" . $middle ."&smallClassCode=". $small .
				"&responseType=large&page=".$page;
		$json = file_get_contents($req);
		// JSONファイルは配列に変換しておく
		$arr = json_decode($json,true);
	} catch(Exception $err) {
		sleep(5);
		$arr = @json_decode($json,true) or die("jsonパースエラーsmall:".$req.$json);
	}
	return $arr;
}

function get_jalan_area() {
	// XMLデータ取得用ベースURL
	$req = "http://www.jalan.net/jalan/doc/jws/data/area.xml";
	// XMLファイルをパースし、オブジェクトを取得(＠でエラーを回避)
	$xml = @simplexml_load_file($req) or die("XMLパースエラー");
	//var_dump ($xml);
	return $xml;
}

function get_jalan_prefecture_hotel($pref, $order, $count, $start) {
	// XMLデータ取得用ベースURL
	// pref：県コード
	// order：並び順（0:宿番号順（初期値）、1:50音順、2:参考料金の安い順、3:参考料金の高い順、4:じゃらんnet人気順）
	// count：出力件数
	// start：スタート位置
	$req = "http://jws.jalan.net/APIAdvance/HotelSearch/V1/?key=ari11949f3d1d2&pref=". 
			$pref ."&order=". $order ."&count=". $count ."&xml_ptn=1&start=".$start;
	// XMLファイルをパースし、オブジェクトを取得(＠でエラーを回避)
	$xml = @simplexml_load_file($req) or die("XMLパースエラー");
	//var_dump ($xml);
	return $xml;
}

function get_jalan_large_area_hotel($large_area, $order, $count, $start) {
	// XMLデータ取得用ベースURL
	// l_area：大エリアコード
	// order：並び順（0:宿番号順（初期値）、1:50音順、2:参考料金の安い順、3:参考料金の高い順、4:じゃらんnet人気順）
	// count：出力件数
	// start：スタート位置
	$req = "http://jws.jalan.net/APIAdvance/HotelSearch/V1/?key=ari11949f3d1d2&l_area=". 
			$large_area ."&order=". $order ."&count=". $count ."&xml_ptn=1&start=".$start;
	// XMLファイルをパースし、オブジェクトを取得(＠でエラーを回避)
	$xml = @simplexml_load_file($req) or die("XMLパースエラー");
	//var_dump ($xml);
	return $xml;
}

function get_jalan_hotel($pref, $order, $count, $start) {
	// XMLデータ取得用ベースURL
	//$req = "http://jws.jalan.net/APIAdvance/HotelSearch/V1/?key=ari11949f3d1d2&pref=". $pref ."&count=100&start=".$start;
	$req = "http://jws.jalan.net/APIAdvance/HotelSearch/V1/?key=ari11949f3d1d2&pref=". 
			$pref ."&order=". $order ."&count=". $count ."&xml_ptn=1&start=".$start;
	//echo $req;
	//http://jws.jalan.net/APIAdvance/HotelSearch/V1/?key=ari11949f3d1d2&pref=010000&count=100&xml_ptn=1
	// XMLファイルをパースし、オブジェクトを取得(＠でエラーを回避)
	$xml = @simplexml_load_file($req) or die("XMLパースエラー");
	//var_dump ($xml);
	return $xml;
}

function get_jalan2($id) {
	// XMLデータ取得用ベースURL
	$req = "http://jws.jalan.net/APILite/HotelSearch/V1/?key=ari11949f3d1d2&h_id=".$id;
	// XMLファイルをパースし、オブジェクトを取得(＠でエラーを回避)
	$xml = @simplexml_load_file($req) or die("XMLパースエラー");
	//var_dump ($xml);
	return $xml;
}

