<?php

/**
 * Data Access Objectクラス定義
 *
 */
class SalesDao {
	/**
	* コンストラクタ
	*/
	public function __construct() {
	}

	public function update_rakuten_area($area_id,
											$large_name,
											$large_code,
											$middle_name,
											$middle_code,
											$small_name,
											$small_code,
											$detail_name,
											$detail_code,
											$status,
											$area_order) {
		global $wpdb;

		try {
			$data = array(
				'large_name' => $large_name,
				'large_code' => $large_code,
				'middle_name' => $middle_name,
				'middle_code' => $middle_code,
				'small_name' => $small_name,
				'small_code' => $small_code,
				'detail_name' => $detail_name,
				'detail_code' => $detail_code,
				'status' => $status,
				'area_order' => $area_order
				);
			$where = array(
				'area_id' => $area_id
				);
			//var_dump($data);
			$wpdb->update( $wpdb->prefix.'sales_rakuten_area', $data, $where);
			return;
		} catch (Exception $err) {
			var_dump($err);
		}
	}

	public function all_jalan_hotel() {
		global $wpdb;

		try {
			// SQLで取得
			//var_dump("SELECT * FROM {$wpdb->prefix}sales_jalan_hotel ORDER BY large_area_code, hotel_order");
			$results = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}sales_jalan_hotel ORDER BY large_area_code, hotel_order LIMIT 0, 10000");
			//var_dump($results);

			return $results;
		} catch (Exception $err) {
			var_dump($err);
		}
	}

	public function count_jalan_hotel() {
		global $wpdb;

		try {
			// SQLで取得
			$results = $wpdb->get_results("SELECT COUNT(*) AS count FROM {$wpdb->prefix}sales_jalan_hotel");
			return $results;
		} catch (Exception $err) {
			var_dump($err);
		}
	}

	public function all_rakuten_hotel() {
		global $wpdb;

		try {
			// SQLで取得
			$results = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}sales_rakuten_hotel ORDER BY hotel_id LIMIT 0, 10000");
			return $results;
		} catch (Exception $err) {
			var_dump($err);
		}
	}

	public function count_rakuten_hotel() {
		global $wpdb;

		try {
			// SQLで取得
			$results = $wpdb->get_results("SELECT COUNT(*) AS count FROM {$wpdb->prefix}sales_rakuten_hotel");
			return $results;
		} catch (Exception $err) {
			var_dump($err);
		}
	}

	public function get_rakuten_hotel($hotel_code) {
		global $wpdb;

		try {
			// SQLで取得
			$results = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}sales_rakuten_hotel 
											WHERE hotel_code = {$hotel_code}");
			return $results;
		} catch (Exception $err) {
			var_dump($err);
		}
	}

	public function insert_rakuten_area($large_name,
											$large_code,
											$middle_name,
											$middle_code,
											$small_name,
											$small_code,
											$detail_name,
											$detail_code,
											$status,
											$area_order) {
		global $wpdb;

		try {
			$set_arr = array(
				'large_name' => $large_name,
				'large_code' => $large_code,
				'middle_name' => $middle_name,
				'middle_code' => $middle_code,
				'small_name' => $small_name,
				'small_code' => $small_code,
				'detail_name' => $detail_name,
				'detail_code' => $detail_code,
				'status' => $status,
				'area_order' => $area_order
				);

			$wpdb->insert( $wpdb->prefix.'sales_rakuten_area', $set_arr);
			return;
		} catch (Exception $err) {
			var_dump($err);
		}
	}

	public function insert_rakuten_hotel($hotel_name,
										$hotel_code,
										$post_code,
										$hotel_address,
										$hotel_type,
										$room_num,
										$prefecture,
										$large_area,
										$small_area,
										$tel,
										$fax,
										$number_of_ratings,
										$rating,
										$service_average,
										$location_average,
										$room_average,
										$equipment_average,
										$bath_average,
										$meal_average,
										$hotel_order,
										$entry_date) {
		global $wpdb;

		try {
			$set_arr = array(
				'hotel_name' => $hotel_name,
				'hotel_code' => $hotel_code,
				'post_code' => $post_code,
				'hotel_address' => $hotel_address,
				'hotel_type' => $hotel_type,
				'room_num' => $room_num,
				'prefecture' => $prefecture,
				'large_area' => $large_area,
				'small_area' => $small_area,
				'tel' => $tel,
				'fax' => $fax,
				'number_of_ratings' => $number_of_ratings,
				'rating' => $rating,
				'service_average' => $service_average,
				'location_average' => $location_average,
				'room_average' => $room_average,
				'equipment_average' => $equipment_average,
				'bath_average' => $bath_average,
				'meal_average' => $meal_average,
				'hotel_order' => $hotel_order,
				'entry_date' => $entry_date
				);
			//var_dump($set_arr);
			$wpdb->insert( $wpdb->prefix.'sales_rakuten_hotel', $set_arr);
			return;
		} catch (Exception $err) {
			var_dump($err);
		}
	}
	public function insert_jalan_area($prefecture_name, $prefecture_code, $prefecture_order) {
		global $wpdb;

		try {
			$set_arr = array(
				'prefecture_name' => $prefecture_name,
				'prefecture_code' => $prefecture_code,
				'prefecture_order' => $prefecture_order
				);

			$wpdb->insert( $wpdb->prefix.'sales_jalan_area', $set_arr);
			return;
		} catch (Exception $err) {
			var_dump($err);
		}
	}

	public function insert_jalan_large_area($large_area_name, $large_area_code, $prefecture_code, $large_area_order) {
		global $wpdb;

		try {
			$set_arr = array(
				'large_area_name' => $large_area_name,
				'large_area_code' => $large_area_code,
				'prefecture_code' => $prefecture_code,
				'large_area_order' => $large_area_order
				);

			$wpdb->insert( $wpdb->prefix.'sales_jalan_large_area', $set_arr);
			return;
		} catch (Exception $err) {
			var_dump($err);
		}
	}

	public function insert_jalan_hotel($hotel_name,
										$hotel_code,
										$post_code,
										$hotel_address,
										$hotel_type,
										$region,
										$prefecture,
										$largeArea,
										$smallArea,
										$prefecture_code,
										$large_area_code,
										$number_of_ratings,
										$rating,
										$hotel_order,
										$entry_date) {
		global $wpdb;
		try {
			$set_arr = array(
				'hotel_name' => $hotel_name,
				'hotel_code' => $hotel_code,
				'post_code' => $post_code,
				'hotel_address' => $hotel_address,
				'hotel_type' => $hotel_type,
				'region' => $region,
				'prefecture' => $prefecture,
				'largeArea' => $largeArea,
				'smallArea' => $smallArea,
				'prefecture_code' => $prefecture_code,
				'large_area_code' => $large_area_code,
				'number_of_ratings' => $number_of_ratings,
				'rating' => $rating,
				'hotel_order' => $hotel_order,
				'entry_date' => $entry_date
				);

			$wpdb->insert( $wpdb->prefix.'sales_jalan_hotel', $set_arr);
			return;
		} catch (Exception $err) {
			var_dump($err);
		}
	}

	public function all_jalan_large_area() {
		global $wpdb;

		try {
			// SQLで取得
			$results = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}sales_jalan_large_area ORDER BY prefecture_code,large_area_order");
			return $results;
		} catch (Exception $err) {
			var_dump($err);
		}
	}

	public function all_jalan_area() {
		global $wpdb;

		try {
			// SQLで取得
			$results = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}sales_jalan_area ORDER BY prefecture_order");
			return $results;
		} catch (Exception $err) {
			var_dump($err);
		}
	}

	public function all_rakuten_area() {
		global $wpdb;

		try {
			// SQLで取得
			$results = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}sales_rakuten_area ORDER BY area_id");
			return $results;
		} catch (Exception $err) {
			var_dump($err);
		}
	}


	public function get_jalan_search($hotel_name) {
		global $wpdb;

		try {
			$results = $wpdb->get_results("SELECT * 
											FROM {$wpdb->prefix}sales_jalan_hotel
											WHERE 
											hotel_name LIKE '%%$hotel_name%%'
											ORDER BY hotel_order");

			return $results;
		} catch (Exception $err) {
			var_dump($err);
		}
	}

	public function get_rakuten_search($hotel_name) {
		global $wpdb;

		try {
			$results = $wpdb->get_results("SELECT * 
											FROM {$wpdb->prefix}sales_rakuten_hotel
											WHERE 
											hotel_name LIKE '%%$hotel_name%%'
											ORDER BY hotel_id");

			return $results;
		} catch (Exception $err) {
			var_dump($err);
		}
	}

}

